from setuptools import setup

# Read requirements.txt
APP = ['osc-knob.py']
DATA_FILES = ['config.ini']
OPTIONS = {
    'argv_emulation': True,
    'packages': ['pynput', 'pythonosc'],
    'iconfile': 'icon/osc-knob.drawio.icns',
}

setup(
    app=APP,
    data_files=DATA_FILES,
    options={'py2app': OPTIONS},
    setup_requires=['py2app'],
)
