import argparse
import threading
import configparser
from pynput import keyboard
from pythonosc import udp_client

# Read configuration from config.ini
config = configparser.ConfigParser()
config.read('config.ini')

# Parse arguments
parser = argparse.ArgumentParser(description="Control Reaper master fader via volume keys.")
parser.add_argument('--debug', action='store_true', default=config.getboolean('DEFAULT', 'debug'))
parser.add_argument('--volume', type=float, default=config.getfloat('DEFAULT', 'volume'))
parser.add_argument('--increment', type=float, default=config.getfloat('DEFAULT', 'increment'))
parser.add_argument('--reaper-ip', type=str, default=config.get('DEFAULT', 'reaper-ip'))
parser.add_argument('--reaper-port', type=int, default=config.getint('DEFAULT', 'reaper-port'))
args = parser.parse_args()

# Variables
current_volume = args.volume

# Define the client for sending OSC messages to Reaper
client = udp_client.SimpleUDPClient(args.reaper_ip, args.reaper_port)

# Counter variable
counter = 0

# Define the counter function to run in the background
def counting_function():
    global counter
    while True:
        counter += 1
        print(f"Count: {counter}")
        threading.Event().wait(1)  # Wait for 1 second

# Start the background timer
background_timer = threading.Thread(target=counting_function)
background_timer.daemon = True  # Set as a daemon thread so it stops when the main program stops
background_timer.start()

# Send initialization message if in debug mode
if args.debug:
    print(f"Program started with initial volume set to {current_volume}. Waiting for volume key presses...")

def on_key_press(key):
    global current_volume

    if args.debug:
        try:
            print(f"Key pressed: {key.char}")
        except AttributeError:
            print(f"Key pressed: {key}")

    # Define the volume control keys
    volume_up = keyboard.Key.media_volume_up
    volume_down = keyboard.Key.media_volume_down

    # Adjust volume based on the detected key press
    if key == volume_up:
        current_volume = min(current_volume + args.increment, 1.0)
        client.send_message("/master/volume", current_volume)
        if args.debug:
            print(f"Increasing volume. New volume: {current_volume}")

    elif key == volume_down:
        current_volume = max(current_volume - args.increment, 0.0)
        client.send_message("/master/volume", current_volume)
        if args.debug:
            print(f"Decreasing volume. New volume: {current_volume}")

with keyboard.Listener(on_press=on_key_press) as listener:
    listener.join()
