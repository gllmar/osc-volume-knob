# osc-volume-knob

## Reaper Volume Control App

A simple macOS application that allows users to control Reaper's master fader volume using media volume keys.
Features

* Control Reaper's master fader volume using macOS media volume keys.
* Adjustable initial volume and volume increment settings.
* Uses OSC (Open Sound Control) to communicate with Reaper.

## Requirements

* macOS
* Reaper with OSC control enabled

## Installation

* Drag and drop the application into your Applications folder.
* In Privacy & Security menu, Add application to Accessibility and to input monitoring
* (Optional) Adjust settings in the config.ini file.

## Configuration

You can adjust various settings in the config.ini file, including:

* Initial volume
* Volume increment value
* Reaper's IP address and port

The config.ini is packaged with the app and can be found in the app's resources.
Usage


## Debugging

* Ensure Reaper is running and has OSC control enabled.
* Start the Reaper Volume Control app.
* Use your media volume keys to adjust Reaper's master fader volume.
* Remove and re add app to Accessibility and to Input Monitoring 

The app supports a debug mode, which prints out useful information to help diagnose issues. This can be enabled by modifying the config.ini file.